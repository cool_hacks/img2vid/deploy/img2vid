FROM elixir:1.13.1-alpine as builder

RUN apk add --no-cache npm nodejs yarn git

WORKDIR /work

# install hex + rebar
RUN mix local.hex --force && \
    mix local.rebar --force

# set build ENV
ENV MIX_ENV="prod"

# install dependencies
COPY ./src/img2vid img2vid

WORKDIR /work/img2vid
RUN HEX_HTTP_CONCURRENCY=1 HEX_HTTP_TIMEOUT=120 mix deps.get --only $MIX_ENV

# compile dependencies first
RUN mix deps.compile

# next prepare assets
COPY ./src/img2vid/priv priv
COPY ./src/img2vid/assets assets
# compile assets
#RUN cd assets/ && yarn install --production && cd ..
#RUN npm --prefix assets install
#RUN npm --prefix assets run build # NOTE: not using npm yet
RUN mix assets.deploy

# Compile the release
RUN mix compile
RUN mix release

# Run the result
FROM alpine:3.15

## Runtime dependencies
## 1. Elixir (bash openssl ncurses-libs)
## 2. ffmpeg
RUN apk add --no-cache bash openssl ncurses-libs libstdc++ ffmpeg

WORKDIR /work/img2vid
COPY --from=builder /work/img2vid/_build/prod/rel/img2vid/ .

CMD ["/work/img2vid/bin/img2vid", "start"]
